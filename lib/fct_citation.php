<?php
/*************************** fct_citation.php  ******************************************************************************/
//    Objet : Librairies de fonction pour calculer, en fonction de chaque type de publication,
//    la citation associée du type AuthFullName_s (producedDate_s). title_s. subTitle_s. publisher_s, page_s, doiId_s, uri_s
//    la fonction générique à utiliser est : getCitation($notice)
//    Elle retourne une chaine de caratères contrairement à ecritCitation($notice,&$section) qui alimente 
//    l'objet RTF pour la génération du fichier word
//    Date : 06/06/2023
//    Auteur : Pierre Pichard - Wouaib.com
/****************************************************************************************************************************/

function getCitationArticle($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) {
		$ssTitre= $notice['subTitle_s'][0].". ";
		$ret .= $ssTitre;
	}
	if (isset($notice['journalTitle_s'])) {
		$journal= $notice['journalTitle_s'].", ";
		$ret .= $journal;
	}
	if (isset($notice['volume_s']))  
		$ret .= $notice['volume_s'];
	else 
		$double=true; 
	// attention double espace si pas de volume
	
	// mettre une virgule apres le volume si pas d'issue_s
	if (isset($notice['issue_s']) && $double) 	
		$ret .= "(".$notice['issue_s'][0]."), ";
	if (isset($notice['issue_s']) && !$double) 	
		$ret .= " (".$notice['issue_s'][0]."), ";
	
	if (!isset($notice['issue_s']))
		$ret .= ", ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationChapitreOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationDirectionOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
	$ret .= "(".$notice['producedDate_s']."). ";
	
	if (isset($notice['title_s'])) 
		$ret .=  $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['conferenceTitle_s'])) 
		$ret .=  $notice['conferenceTitle_s'].", ";
	
	if (isset($notice['city_s'])) 
		$ret .=  $notice['city_s'].", ";
	
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s'])." ";
	
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceEndDate_s'])) 
		$ret .=  "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."). ";
	
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationCommunication($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
	$ret .= "(".$notice['producedDate_s']."). ";
	
	if (isset($notice['title_s'])) 
		$ret .=  $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['conferenceTitle_s'])) 
		$ret .=  "Presented at ".$notice['conferenceTitle_s'].", ";
	
	if (isset($notice['city_s'])) 
		$ret .=  $notice['city_s'].", ";
	
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s'])." ";
	
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceEndDate_s'])) 
		$ret .=  "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."), ";
	
	if ($notice['invitedCommunication_s'] == "1")
		$ret .=  ' Conférence invitée. ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationBrevet($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s']))
		$ret .= $notice['subTitle_s'][0].". ";
		
	if (isset($notice['number_s']))
		$ret .= "(Brevet n°: ".$notice['number_s'][0]."). ";
		
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s']).", ";
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationRapport($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .= $notice['subTitle_s'][0].". ";
		
	if (isset($notice['authorityInstitution_s'])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."), ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['number_s']))
		$ret .= $notice['number_s'][0].', ';
		
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationChapitreRapport($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';
		
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationPrepubli($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['serie_s'][0]))
		$ret .=  $notice['serie_s'][0] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 	
	
	if (isset($notice['arxivId_s']))
		$ret .= "n° arxiv: ".$notice['arxivId_s'].", ";
		
	if (isset($notice['biorxivId_s']))
		$ret .= "n° biorxiv: ".$notice['biorxivId_s'][0].", ";	
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationTheseHdrMemoire($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0]." ";
	
	if (isset($notice['authorityInstitution_s'][0])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."). ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationCours($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0]." ";
	
	if (isset($notice['lectureName_s']) || isset($notice['lectureType_s']) || isset($notice['authorityInstitution_s'])){
		$cours = "(". $notice['lectureName_s'] .", ". getLectureType($notice['lectureType_s']) .", ". $notice['authorityInstitution_s'][0] .")";
		$cours = str_replace('(, ','(',$cours);
		$cours = str_replace(', )',')',$cours);
		$ret .= $cours.", ";
	}
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationLogiciel($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationImageCarte($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['country_s'])) 
		$ret .= code_to_country($notice['country_s']).", ";
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationSonVideo($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";
	
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationAutre($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['authorityInstitution_s'][0])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."). ";
	
	if (isset($notice['country_s'])) 
		$ret .= code_to_country($notice['country_s']).", ";
	
	if (isset($notice['serie_s'][0]))
		$ret .=  $notice['serie_s'][0] . ', ';
	
	if (isset($notice['journalTitle_s']))
		$ret .= $notice['journalTitle_s'].", ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';	
	
	if (isset($notice['lectureName_s']) || isset($notice['lectureType_s']) || isset($notice['authorityInstitution_s'])){
		$cours = "(". $notice['lectureName_s'] .", ". getLectureType($notice['lectureType_s']) .", ". $notice['authorityInstitution_s'][0] .")";
		$cours = str_replace('(, ','(',$cours);
		$cours = str_replace(', )',')',$cours);
		$ret .= $cours.", ";
	}
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';	
	
	if (isset($notice['arxivId_s']))
		$ret .= "n° arxiv: ".$notice['arxivId_s'].", ";
		
	if (isset($notice['biorxivId_s']))
		$ret .= "n° biorxiv: ".$notice['biorxivId_s'][0].", ";	
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";	

	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

/* ajout des fonctions de citations liés aux nouveaux types hal 2022 */
function getCitationCommune($notice){
	$ret = getAuteursHceres($notice). ' ';
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	$ret .= $notice['title_s'][0].". ";
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	return $ret;
}

function getCitationProceeding($notice){
	$ret = getCitationCommune($notice);
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitationBlogTrad($notice){
	$ret = getCitationCommune($notice);
	if (isset($notice['description_s']))
		$ret .= $notice['description_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice['halId_s']);
	$ret .= $lien;
	
	return $ret;
}

function getCitation($notice){
	switch($notice['docType_s']){
		case 'ART':		  return getCitationArticle($notice);							break;
		case 'REPORT':    return getCitationRapport($notice);    						break;
		case 'OUV':		  return getCitationOuvrage($notice);							break;
		case 'DOUV':      return getCitationDirectionOuvrage($notice);		            break;
		case 'COUV':      return getCitationChapitreOuvrage($notice);					break;
		case 'COMM':      return getCitationCommunication($notice);						break;
		case 'HDR':		  return getCitationTheseHdrMemoire($notice);					break;					
		case 'OTHER':	  return getCitationAutre($notice);								break;
		case 'SOFTWARE':  return getCitationLogiciel($notice);							break;
		case 'PATENT':    return getCitationBrevet($notice);							break;
		case 'LECTURE':   return getCitationCours($notice);								break;
		case 'VIDEO':     return getCitationSonVideo($notice);							break;
		case 'MAP':       return getCitationImageCarte($notice);						break;
		case 'IMG':       return getCitationImageCarte($notice);						break;
		case 'SON':       return getCitationSonVideo($notice);							break;
		case 'POSTER':    return getCitationCommunication($notice);						break;
		case 'MEM':       return getCitationTheseHdrMemoire($notice);					break;
		case 'CREPORT':   return getCitationChapitreRapport($notice);					break;
		case 'THESE':     return getCitationTheseHdrMemoire($notice);					break;
		case 'UNDEFINED': return getCitationPrepubli($notice);							break;
		/* nouveaux type std 2022 */
		case 'MEMLIC':      return getCitationTheseHdrMemoire($notice);					break;
		case 'PROCEEDINGS': return getCitationProceeding($notice);  					break;
		case 'ISSUE':	    return getCitationArticle($notice);      					break;
		case 'TRAD':	    return getCitationBlogTrad($notice);						break;
		case 'BLOG':	    return getCitationBlogTrad($notice);						break;
		case 'NOTICE':		return getCitationChapitreOuvrage($notice);					break;
		default :         	return getCitationArticle($notice);                          break;
	}		
}

?>