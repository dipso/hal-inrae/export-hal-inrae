<?php
/*******************************************************************************************************************************************/
/*
/*              PROGRAMME D'EXPORT CSS POUR L'INRAE
/*
/*   Usage : appel ajax depuis assets/js/inrae.js
/*   Paramatres : idHal de l'auteur, année de début, année de fin, booléen renommage scientifiq des auteurs Nom, P.
/*   Résultat : production d'un fichier rtf dans le répertoire docs/ comportant les notices résultantes de l'interrogation de l'API HAL
/*   Développement : Pierre Pichard - wouaib.com
/*   Date : 2021
/*
/*******************************************************************************************************************************************/


$debug = false;
$h =     fopen("debug.log","w");
$msg = "ok";
$noticesTraitees = array();  // stockage de toutes les publis traitées, pour remplir la section des publis non ventilées
$listeChamps = "halId_s,title_s,authFullName_s,authLastName_s,authFirstName_s,producedDate_s,producedDateY_i,docType_s,docSubType_s,inra_publicVise_local_s,peerReviewing_s,invitedCommunication_s,subTitle_s,bookTitle_s,journalTitle_s,volume_s,issue_s,page_s,publisher_s,doiId_s,uri_s,arxivId_s,biorxivId_s,authorityInstitution_s,number_s,serie_s,conferenceTitle_s,city_s,country_s,conferenceStartDate_s,conferenceEndDate_s,lectureName_s,reportType_s,lectureType_s,otherType_s,description_s,linkExtUrl_s,fileMain_s,openAccess_bool,collCode_s,authFullNamePersonIDIDHal_fs";


// Récuperation des données du formulaire
$idhal    = htmlspecialchars(strip_tags($_POST['idHal']),    ENT_QUOTES);
$anneedeb = htmlspecialchars(strip_tags($_POST['dateDeb']),  ENT_QUOTES);
$anneefin = htmlspecialchars(strip_tags($_POST['dateFin']),  ENT_QUOTES);
$formeAut = htmlspecialchars(strip_tags($_POST['formeAut']), ENT_QUOTES);  // 1 pour cocher, 0 sinon

@include_once("../lib/functions.php");

if (isset($idhal) && isset($anneedeb) && isset($anneefin)) {
  
	$url = "http://api.archives-ouvertes.fr/search/?wt=json&q=authIdHal_s:".$idhal."&rows=100000&fq=producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&fl=".$listeChamps;
	$linkHAL = '<br><a class="btn btn-secondary" href="'. $url. '" target="_blank">Afficher la requête API (format json)</a><br>';
	
	if ($anneedeb=="") $annedeb="*";
	if ($anneefin=="") $anneefin="*";
	$urlHAL = "https://hal.inrae.fr/search/index/?q=authIdHal_s:".$idhal."+producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&submitType_s=notice+OR+file+OR+annex&rows=300";
	$linkInrae = '<br><a class="btn btn-secondary" href="'. $urlHAL. '" target="_blank">Lancer la recherche sur HAL - INRAE</a>';
		
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if (isset ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
	}
	$resultats = curl_exec($ch);
	curl_close($ch);
	
	$data = json_decode($resultats,true);
	$notices = $data["response"]["docs"];
		
	if (count($notices)==0) {
		$retourne[1] = "Aucune notice ne correspond à ces critères. Veuillez saisir un idHAL correct SVP.";
		$retourne[2] = "Erreur";
		$error = true;
		echo json_encode($retourne);
		exit;
	} else {
		$msg = count($notices)." notices correspondent aux critères";
		$error = false;
	}
	
	
	
	if ($debug)
		fwrite($h,print_r($_GET,true));
	
	$dir = dirname(__FILE__);
	require_once $dir . '/lib/PHPRtfLite.php';

	// register PHPRtfLite class loader
	PHPRtfLite::registerAutoloader();

	$rtf = new PHPRtfLite();
	$fontStyleTitre = new PHPRtfLite_Font();
	$fontStyleTitreBold = new PHPRtfLite_Font();
	$fontStyleTitreBold->setBold();
	$fontStyleIntitule = new PHPRtfLite_Font(10,'Times New Roman','#00A3A6','#ffffff');
	$fontStyleIntitule->setItalic();
	$fontItalic = new PHPRtfLite_Font(10,'Times New Roman','#000000','#ffffff');
	$fontItalic->setItalic();
	$fontBleu = new PHPRtfLite_Font(10,'Times New Roman','#00A3A6','#ffffff');
	$fontOrange = new PHPRtfLite_Font(10,'Times New Roman','#ec6d42','#ffffff');
	$fontViolet = new PHPRtfLite_Font(10,'Times New Roman','#423089','#ffffff');
	$fontViolet->setItalic();
	
	$section = $rtf->addSection();
	
	// Ventilation par type de document
	foreach ($notices as $notice) {
		switch($notice['docType_s']){
			case 'ART':		    $articles[]   =  $notice;		break;
			case 'REPORT':      $rapports[]   =  $notice;       break;
			case 'OUV':		    $ouvrages[]   =  $notice;		break;
			case 'COUV':        $chapitres[]  =  $notice;		break;
			case 'COMM':        $comms[]      =  $notice;		break;
			case 'HDR':		    $hdr[]        =  $notice;		break;					
			case 'OTHER':	    $others[]     =  $notice;		break;
			case 'SOFTWARE':    $logs[]       =  $notice;		break;
			case 'PATENT':      $brevets[]    =  $notice;		break;
			case 'LECTURE':     $cours[]      =  $notice;		break;
			case 'VIDEO':       $videos[]     =  $notice;		break;
			case 'MAP':         $cartes[]     =  $notice;		break;
			case 'IMG':         $images[]     =  $notice;		break;
			case 'SON':         $sons[]       =  $notice;		break;
			case 'POSTER':      $posters[]    =  $notice;		break;
			case 'UNDEFINED':   $prepublis[]  =  $notice;		break;
			case 'THESE':	    $theses[]     =  $notice;		break;
			case 'PROCEEDINGS': $proceedings[]=  $notice;		break;   /* nouveaux types 2023 */
			case 'ISSUE':       $issues[]     =  $notice;       break;
			case 'TRAD':        $trads[]      =  $notice;       break;
			case 'BLOG':        $blogs[]      =  $notice;       break;
			case 'NOTICE':      $encyclos[]   =  $notice;       break;
			case 'CREPORT':     $creports[]   =  $notice;       break;
			case 'MEM':			$memoires[]	  =  $notice;       break;
			case 'MEMLIC':		$memoires[]	  =  $notice;       break;
			default :           $autres[]     =  $notice;		break;  // doit gérer le cas des MEMLIC
		}
	}
	
	// ***********************************************************//
	// Remplissage du document Word
	// 1. Production de connaissances
	//
	////////////////////////////////////////////////////////////////
	
	// Les Articles
	$section->writeText('II LISTE DES REALISATIONS ET ACTIVITES<br />',$fontStyleTitreBold);
	$section->writeText('<br />');
	$section->writeText('<br />A. Production de connaissances',$fontStyleTitreBold);
	$section->writeText('<br />Vous pouvez en quelques lignes dans un petit paragraphe à cet endroit expliciter :',$fontBleu);
	$section->writeText('<br />   - votre stratégie de valorisation,',$fontBleu);
	$section->writeText('<br />   - les règles de signature (si vous en avez),',$fontBleu);
	$section->writeText('<br />   - le choix de vos supports de valorisation,',$fontBleu);
	$section->writeText('<br />   - votre politique sur le libre accès,',$fontBleu);
	$section->writeText('<br />   - vos rôles dans les différentes productions listées, en partant du vocabulaire proposé par CreDit.',$fontBleu);
	
	$section->writeText('<br /><br />   A.1. Production de connaissances originales, reconnues dans la/les communauté(s) scientifique(s) de référence',$fontStyleTitre);
	$section->writeText('<br />Les productions doivent être listées sous forme d’une référence bibliographique précisant le lien vers le site de l’éditeur (DOI), le lien vers la référence dans une archive ouverte (par ex dans HAL INRAE) et le texte intégral déposé dans l’archive. A noter : l’export HAL produit automatiquement les références sous cette forme.',$fontBleu);
	
	$section->writeText('<br /><br />      A.1.1 Articles à destination des communautés scientifiques, validés par les pairs et publiés dans une revue à comité de lecture ou dans un entrepôt de documents en accès libre (comme par exemple les Peer Community In)<br />',$fontStyleTitre);
	
	// Articles A.1.1
	if (isset($articles)){
		foreach($articles as $article){
			
			if ($article['inra_publicVise_local_s'][0] == "SC"  && $article['peerReviewing_s'] == "1" && $article['docSubType_s'] != "ARTREV" && $article['docSubType_s'] != "DATAPAPER" && $article['docSubType_s'] != "BOOKREVIEW"){
				ecritArticle($article,$section,false,false,true);
				$noticesTraitees[] = $article['halId_s'];    // on stocke son halId pour ne pas la ressortir dans la dernière section du fichier word
			}	
		}
	}
	
	// Libelles articles A.1.2 
	$section->writeText('<br /><br />      A.1.2 Manuscrits déposés dans un entrepôt de documents d’accès libre non validés par les pairs (« preprints ») ou articles soumis à une revue ou déposés dans un entrepôt de documents d’accès libre en attente de validation par les pairs<br />',$fontStyleTitre);		
	if (isset($prepublis)){
		foreach($prepublis as $prepubli){
			if ( ($prepubli['inra_publicVise_local_s'][0] == "SC") || ($prepubli['arxivId_s'] != "" || $prepubli['biorxivId_s'] != "") ){
				ecritPreprintWp($prepubli,$section,false);
				$noticesTraitees[] = $prepubli['halId_s'];   
			}	
		}
	}
	
	
	// Libelles articles A.1.3
	$section->writeText('<br /><br />      A.1.3 Articles à destination des communautés scientifiques publiés dans une revue sans comité de lecture<br />',$fontStyleTitre);
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['inra_publicVise_local_s'][0] == "SC" && $article['peerReviewing_s'] == "0"){
				ecritArticle($article,$section,false,false,false);
				$noticesTraitees[] = $article['halId_s'];
			}
		}
	}
	
	
	// Libelles articles A.1.4
	$section->writeText('<br /><br />      A.1.4 Articles de synthèse publiés dans une revue avec comité de lecture : préciser si c’est sur invitation<br />',$fontStyleTitre);
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['docSubType_s'] == "ARTREV" && $article['inra_publicVise_local_s'][0] == "SC"  && $article['peerReviewing_s'] == "1" ){
				ecritArticle($article,$section,false,false,true);  
				$noticesTraitees[] = $article['halId_s'];
			}
		}
	}
	
	// DATAPAPER
	$section->writeText('<br /><br />      A.1.5 Datapapers, pre-registrations<br />',$fontStyleTitre);
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['docSubType_s'] == "DATAPAPER" && $article['inra_publicVise_local_s'][0] == "SC"){
				ecritArticle($article,$section,false,false,true);  
				$noticesTraitees[] = $article['halId_s'];
			}
		}
	}
	
	// Ouvrages complets et/ou direction d’ouvrages ou d’actes
	$section->writeText('<br /><br />      A.1.6 Ouvrages complets et/ou direction d’ouvrages ou d’actes<br />',$fontStyleTitre);	
	if (isset($proceedings)){
		foreach($proceedings as $proceeding){
			if ($proceeding['inra_publicVise_local_s'][0] == "SC") {
				ecritProceeding($proceeding,$section,true);
				$noticesTraitees[] = $proceeding['halId_s'];
			}
		}
	}
	if (isset($ouvrages)){
		foreach($ouvrages as $ouvrage){
			if ($ouvrage['inra_publicVise_local_s'][0] == "SC"){
				ecritOuv($ouvrage,$section,true,false);
				$noticesTraitees[] = $ouvrage['halId_s'];
			}
		}
	}
	
	
	
	// chapitres
	$section->writeText('<br /><br />      A.1.7 Chapitres d’ouvrages<br />',$fontStyleTitre);
	if (isset($chapitres)){
		foreach($chapitres as $chapitre){
			if ($chapitre['inra_publicVise_local_s'][0] == "SC"){
				ecritChapitreEncyclo($chapitre,$section);
				$noticesTraitees[] = $chapitre['halId_s'];
			}
		}
	}
	
	$section->writeText('<br /><br />      A.1.8 Relecture d’articles : préciser s’il s’agit d’open peer reviews',$fontStyleTitre);
	$section->writeText('<br />Lister les journaux/platesformes pour lesquel.les vous avez relu des articles. Si les reviews sont accessibles en libre accès, citez la référence à l’aide du DOI.',$fontBleu);
	
	$section->writeText('<br /><br />      A.1.9 Compte-rendu d’ouvrage ou note de lecture ou préface d’actes<br />',$fontStyleTitre);
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['docSubType_s'] == "BOOKREVIEW" && $article['inra_publicVise_local_s'][0] == "SC"){
				ecritArticle($article,$section);  
				$noticesTraitees[] = $article['halId_s'];
			}
		}
	}
	
	
	// A.1.10      Habilitation à diriger des recherches
	$section->writeText('<br /><br />      A.1.10 Habilitation à diriger des recherches<br />',$fontStyleTitre);
	if (isset($hdr)){
		foreach($hdr as $hdrNotice){
			ecritHdr($hdrNotice,$section);
			$noticesTraitees[] = $hdrNotice['halId_s'];			
		}
	}
	
	///////////////////////////////////////////
	/// Ajout de sections supplémentaires pour compatibilité OpenOffice
	////////////////////////////////////////
	//$section = $rtf->addSection();
	
	
	// A.1.11      Communications à des congrès et colloques : communication orale
	$section->writeText('<br /><br />      A.1.11 Communications à des congrès et colloques : précisez si conférence invitée, keynote, communication orale, poster<br />',$fontStyleTitre);
	if (isset($comms)){
		foreach($comms as $comm){
			if ($comm['inra_publicVise_local_s'][0] == "SC"){
				ecritComm($comm,$section);
				$noticesTraitees[] = $comm['halId_s'];
			}
		}
	}
	if (isset($posters)){
		foreach($posters as $poster){
			if ($poster['inra_publicVise_local_s'][0] == "SC"){
				ecritPoster($poster,$section,true);
				$noticesTraitees[] = $poster['halId_s'];
			}
		}
	}
	
	
	// 1.2      Communication à des congrès et colloques : préface d’acte
	$section->writeText('<br /><br />   A.2. Produits pour la recherche mis à disposition de communautés scientifiques (logiciels, bases de données, matériels biologiques, etc.)',$fontStyleTitre);
	$section->writeText('<br />Indiquer si ces données sont associées à un identifiant pérenne (DOI : digital object identifier) permettant de les référencer, de les citer. Indiquer si ces données sont « FAIR ».',$fontBleu);
	$section->writeText('<br />Si les jeux de données sont publiés, donner la référence sous la forme d’une citation bibliographique avec le lien qui permet d’accéder au contenu.',$fontBleu);
	$section->writeText('<br />Indiquer si le code, logiciel et/ou algorithme est ouvert (et sous quelle licence).',$fontBleu);
	$section->writeText('<br /><br />      A.2.1 Développements informatiques à destination des communautés scientifiques (applications, bases de données, ontologies, terminologies, bibliothèques, plugins, sites web, couches graphiques, outils aide à la décision…)<br />',$fontStyleTitre);
	if (isset($logs)){
		foreach($logs as $log){
			if ($log['inra_publicVise_local_s'][0] == "SC"){
				ecritLogiciel($log,$section);
				$noticesTraitees[] = $log['halId_s'];
			}
		}
	}
	$section->writeText('<br /><br />      A.2.2 Jeux de données',$fontStyleTitre);
	$section->writeText('<br />Certaines données déjà incluses dans d’autres types de productions ne sont pas nécessairement à re-lister, notamment si elles sont nombreuses (comme des données de génomique).',$fontBleu);
	$section->writeText('<br /><br />      A.2.3 Matériels biologiques',$fontStyleTitre);
	
	
	// 1.3 Elaboration et animation de projets de recherche (académique, participatif, avec des partenaires privés ou publics)
	$section->writeText('<br /><br />   A.3. Elaboration et animation de projets de recherche (académique, participatif, avec des partenaires privés ou publics)',$fontStyleTitre);
	$section->writeText('<br />Préciser votre rôle : coordinateur/coordinatrice, partenaire, responsable de « work package ». Indiquer l’année d’obtention ou de la mise en œuvre du projet, de la démarche, du consortium. Si vous le souhaitez, vous pouvez également indiquer les projets refusés. Possibilité d’indiquer vos rapports de fin de contrats dans la rubrique B.1.1.',$fontBleu);
			
	$section->writeText('<br /><br />      A.3.1 Projets de recherches en partenariat avec un ou plusieurs partenaires académiques, socio-économiques ou pouvoirs publics',$fontStyleTitre);
	$section->writeText('<br /><br />      A.3.2 Mise en œuvre de démarches de recherche participative associant des acteurs/actrices non-scientifiques-professionnel.les, individuel.les ou en groupe, participant de façon active et délibérée',$fontStyleTitre);
	$section->writeText('<br /><br />      A.3.3 Coordination ou participation à l’animation de consortia multi-acteurs nationaux ou internationaux (GIS, …)',$fontStyleTitre);
	
	
	
	// 1.4   Collaborations scientifiques, formalisées ou en cours 
	$section->writeText('<br /><br />   A.4. Collaborations scientifiques, formalisées ou en cours d’élaboration',$fontStyleTitre);
	$section->writeText('<br />Préciser les dates de mise en œuvre et si, éventuellement, elles s\'inscrivent dans un projet et si des publications ou valorisations y sont associées.',$fontBleu);
	$section->writeText("<br /><br />      A.4.1 Lister les collaborations au sein de l’unité, d'INRAE et nationales hors INRAE",$fontStyleTitre);
	$section->writeText('<br /><br />      A.4.2 Lister les collaborations internationales',$fontStyleTitre);
	$section->writeText('<br /><br />      A.4.3 Participation (sans responsabilité) à des réseaux thématiques, disciplinaires, sociétés savantes, organisation de congrès…',$fontStyleTitre);
			
	
	///////////////////////////////////////////
	/// Ajout de sections supplémentaires pour compatibilité OpenOffice
	////////////////////////////////////////
	//$section = $rtf->addSection();
	
	//////////////////////////////////////////////////////////////////////////////////
	//
	//  B. Expertise et mobilisation des connaissances
	//
	//////////////////////////////////////////////////////////////////////////////////
	$section->writeText('<br /><br/>B. Expertise et mobilisation des connaissances',$fontStyleTitreBold);
	$section->writeText('<br /><br />   B.1 Expertise scientifique et technique auprès des décisionnaires (INRAE, pouvoirs publics nationaux ou internationaux, collectivités territoriales, des agences, etc.)',$fontStyleTitre);
	$section->writeText('<br />Les activités d’expertise, de prospective et d’appui aux politiques publiques se développent en étroite synergie avec les travaux de recherche, afin de renforcer le continuum recherche – innovation – appui aux politiques publiques.',$fontBleu);
	$section->writeText('<br />L’objectif est de valoriser les connaissances scientifiques et techniques pour répondre et accompagner les acteurs/actrices responsables de la conception, de la mise en œuvre et de l’évaluation des politiques publiques.',$fontBleu);
	$section->writeText('<br /><br />      B.1.1 Rapports à destination des décisionnaires, financeurs (rapports d’étude, rapports d’expertise scientifique, rapports de fin de contrat, rapports de prospective, working paper, rapports techniques, états de l’art, comptes-rendus de mission…)<br />',$fontStyleTitre);
	
	if (isset($rapports)){
		foreach($rapports as $rapport){
			ecritRapport($rapport,$section,true);
			$noticesTraitees[] = $rapport['halId_s'];
		}			
	}

	if (isset($prepublis)){
		foreach($prepublis as $prepubli){
			if ($prepubli['docSubType_s']=="WORKINGPAPER"){
				ecritPreprintWp($prepubli,$section,true);
				$noticesTraitees[] = $prepubli['halId_s'];
			}
		}
	}
	
	
	// 2.1.2 Ouvrages de synthèse à vocation de transfert
	$section->writeText('<br /><br />      B.1.2 Ouvrages, direction d’ouvrage ou d’actes et chapitres techniques ou de synthèse à vocation de transfert<br />',$fontStyleTitre);
	// ouvrage + type de la publication=synthèse + public visé=(’technique’ OR ‘pouvoirs publics’ OR ‘étudiants’ OR ‘autre’)
	if (isset($ouvrages)){
		foreach($ouvrages as $ouvrage){
			if ($ouvrage['inra_publicVise_local_s'][0] == "PP" || $ouvrage['inra_publicVise_local_s'][0] == "TE"){  
				ecritOuv($ouvrage,$section,true,true);
				$noticesTraitees[] = $ouvrage['halId_s'];
			}
		}
	}
	
	if (isset($proceedings)){
		foreach($proceedings as $proceeding){
			if ($proceeding['inra_publicVise_local_s'][0] == "PP" || $proceeding['inra_publicVise_local_s'][0] == "TE") {
				ecritProceeding($proceeding,$section,true);
				$noticesTraitees[] = $proceeding['halId_s'];
			}			
		}
	}
	
	
	if (isset($chapitres)){
		foreach($chapitres as $chapitre){
			if ($chapitre['inra_publicVise_local_s'][0] == "PP" || $chapitre['inra_publicVise_local_s'][0]=="TE"){
				ecritChapitreEncyclo($chapitre,$section,true);   
				$noticesTraitees[] = $chapitre['halId_s'];
			}
		}
	}
	
	
	
	// B.1.3  Recommandations et propositions de normes ou règlementations
	$section->writeText('<br /><br />      B.1.3 Recommandations et propositions de normes ou réglementations<br />',$fontStyleTitre);	
	$section->writeText('<br /><br />      B.1.4 Organisations et actes de séminaires à destination d’acteurs publics',$fontStyleTitre);
	
	$section->writeText('<br /><br />      B.1.5 Communications techniques ou de transfert<br />',$fontStyleTitre);
	if (isset($comms)){
		foreach($comms as $comm){
			if ($comm['inra_publicVise_local_s'][0] == "PP" || $comm['inra_publicVise_local_s'][0] == "TE"){
				ecritComm($comm,$section);
				$noticesTraitees[] = $comm['halId_s'];
			}
		}
	}
	if (isset($posters)){
		foreach($posters as $poster){
			if ($poster['inra_publicVise_local_s'][0] == "PP" || $poster['inra_publicVise_local_s'][0] == "TE"){
				ecritPoster($poster,$section,true);
				$noticesTraitees[] = $poster['halId_s'];
			}
		}
	}
	
	$section->writeText('<br /><br />      B.1.6 Participation à des comités (éthiques…), instances d’INRAE ou des partenaires académiques de l’Institut (conseil scientifique, conseil de gestion, CAP, CSS, CEI, etc.)',$fontStyleTitre);
	
	// 2.2. Valorisation des connaissances scientifiques en appui à l’innovation
	$section->writeText('<br /><br />   B.2 Valorisation des connaissances scientifiques en appui à l’innovation',$fontStyleTitre);
	$section->writeText('<br />Lister dans ce cadre les projets auxquels vous participez et les produits issus de ces activités.',$fontBleu);
	$section->writeText('<br /><br />      B.2.1 Projets tournés vers l’innovation (preuve du concept, pré-maturation, prototype, essais pilotes, démonstrateur préindustriel…); contribution à des dynamiques d’innovation ouverte (Living Lab, Hackathon ou Challenge, …)',$fontStyleTitre);
	$section->writeText('<br /><br />      B.2.2 Déclarations d’inventions et de résultats valorisables et protection intellectuelle des résultats : savoir-faire, matériel biologique, modèle déposé, logiciel, bases de données, brevet, certificat d’obtention végétale…<br />',$fontStyleTitre);
	// logiciel + public visé=’technique’ OR ‘pouvoirs publics’ OR ‘étudiants’ OR ‘autre’
	if (isset($logs)){
		foreach($logs as $log){
			if ($log['inra_publicVise_local_s'][0] != "SC" && $log['inra_publicVise_local_s'][0] != "ET" && $log['inra_publicVise_local_s'][0] != "GP"){
				ecritLogiciel($log,$section);
				$noticesTraitees[] = $log['halId_s'];
			}
		}
	}
	
	// brevet + public visé=’technique’ OR ‘pouvoirs publics’ OR ‘étudiants’ OR ‘autre’
	if (isset($brevets)){
		foreach($brevets as $brevet){
			ecritBrevet($brevet,$section);
			$noticesTraitees[] = $brevet['halId_s'];				
		}
	}
	
	$section->writeText('<br /><br />      B.2.3 Création ou participation à la création d’une entreprise pour valoriser ses résultats de recherche, ou collaborer avec une start-up, pour lever des verrous scientifiques et technologiques et permettre l’émergence de start-up innovantes grâce aux recherches de l’institut ; recherche de bénéficiaires dans le cadre de l’ouverture de codes et logiciels',$fontStyleTitre);
	$section->writeText('<br /><br />      B.2.4 Expertise pour des organisations professionnelles ou des acteurs/actrices privé.es',$fontStyleTitre);
	$section->writeText('<br /><br />      B.2.5 Publication et valorisation dans des journaux et plateformes destinés aux professionnel.les ; alimentation de data market places<br />',$fontStyleTitre);
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['inra_publicVise_local_s'][0] == "TE"){
				ecritArticle($article,$section,false,false,false);
				$noticesTraitees[] = $article['halId_s'];
			}	
		}
	}
	
	// 2.3 Expertise scientifique auprès de la communauté scientifique nationale et internationale
	$section->writeText('<br /><br />   B.3 Expertise scientifique auprès de la communauté scientifique nationale et internationale',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.1 Participation à des comités de rédactions éditoriaux',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.2 Participation à des plateformes, initiatives de sciences ouvertes',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.3 Evaluation de projets scientifiques, de rapports de thèse, d’HDR et d’entités de recherche à l’échelle nationale ou internationale',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.4 Participation à des comités de thèses, des instances de recrutement, des instances d’évaluation individuelle ou de collectifs, jurys de concours, jurys de sélection ; participation à des conseils scientifiques',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.5 La représentation de l’institut dans des instances ou organisations nationales, européennes ou internationales impliquées dans des réflexions stratégiques (ANR, alliances, PEER, Belmont, …)',$fontStyleTitre);
	$section->writeText('<br /><br />      B.3.6 Organisation de congrès, participation à des comités d’organisation',$fontStyleTitre);
	// 2.4 Contribution à l’analyse de l’impact sociétal de la recherche
	$section->writeText('<br /><br />   B.4 Contribution à l’analyse de l’impact sociétal de la recherche',$fontStyleTitre);
	$section->writeText('<br /><br />      B.4.1 Déploiement de la méthode ASIRPA (Analyse des Impacts de la Recherche Publique Agronomique)',$fontStyleTitre);
	$section->writeText('<br /><br />      B.4.2 Autres analyses, documents de synthèse, études sur la contribution d’INRAE à des impacts sociétaux avérés',$fontStyleTitre);
	// 2.5 Contribution aux débats d’idées et actions d’information
	$section->writeText('<br /><br />   B.5 Contribution aux débats d’idées et actions d’information',$fontStyleTitre);
	$section->writeText('<br />Article, communication, poster, ouvrage, chapitre d’ouvrage, direction d’ouvrage ou d’actes, vidéo, son, logiciel, image, carte…<br/>',$fontBleu);
	
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['inra_publicVise_local_s'][0] == "GP"){
				ecritArticle($article,$section,true,true,false);
				$noticesTraitees[] = $article['halId_s'];
			}	
		}
	}
	if (isset($comms)){
		foreach($comms as $comm){
			if ($comm['inra_publicVise_local_s'][0] == "GP"){
				ecritComm($comm,$section,true);
				$noticesTraitees[] = $comm['halId_s'];
			}
		}
	}
	if (isset($proceedings)){
		foreach($proceedings as $proceeding){
			if ($proceeding['inra_publicVise_local_s'][0] == "GP"){
				ecritProceeding($proceeding,$section,true);
				$noticesTraitees[] = $proceeding['halId_s'];
			}
		}
	}
	if (isset($encyclos)){
		foreach($encyclos as $encyclo){
			if ($encyclo['inra_publicVise_local_s'][0] == "GP"){
				ecritChapitreEncyclo($encyclo,$section,true);
				$noticesTraitees[] = $encyclo['halId_s'];
			}
		}
	}
	if (isset($posters)){
		foreach($posters as $poster){
			if ($poster['inra_publicVise_local_s'][0] == "GP"){
				ecritPoster($poster,$section,true);
				$noticesTraitees[] = $poster['halId_s'];
			}
		}
	}
	if (isset($ouvrages)){
		foreach($ouvrages as $ouvrage){
			if ($ouvrage['inra_publicVise_local_s'][0]=="GP"){
				ecritOuv($ouvrage,$section,true,true);
				$noticesTraitees[] = $ouvrage['halId_s'];
			}
		}
	}
			
	if (isset($chapitres)){
		foreach($chapitres as $chapitre){
			if ($chapitre['inra_publicVise_local_s'][0] == "GP"){
				ecritChapitreEncyclo($chapitre,$section,true);
				$noticesTraitees[] = $chapitre['halId_s'];
			}
		}
	}
	
	if (isset($issues)){
		foreach($issues as $issue){
			if ($issue['inra_publicVise_local_s'][0] == "GP"){
				ecritIssue($issue,$section,true);
				$noticesTraitees[] = $issue['halId_s'];
			}
		}
	}
	
	if (isset($videos)){
		foreach($videos as $video){
			if ($video['inra_publicVise_local_s'][0] == "GP"){
				ecritVideoSon($video,$section,true);
				$noticesTraitees[] = $video['halId_s'];
			}
		}
	}
	if (isset($sons)){
		foreach($sons as $son){
			if ($son['inra_publicVise_local_s'][0] == "GP"){
				ecritVideoSon($son,$section,true);
				$noticesTraitees[] = $son['halId_s'];
			}
		}
	}
	if (isset($logs)){
		foreach($logs as $log){
			if ($log['inra_publicVise_local_s'][0] == "GP"){
				ecritLogiciel($log,$section);
				$noticesTraitees[] = $log['halId_s'];
			}
		}
	}
	if (isset($images)){
		foreach($images as $image){
			if ($image['inra_publicVise_local_s'][0] == "GP"){
				ecritImage($image,$section,true);
				$noticesTraitees[] = $image['halId_s'];
			}
		}
	}
	if (isset($cartes)){
		foreach($cartes as $carte){
			if ($carte['inra_publicVise_local_s'][0] == "GP"){
				ecritCarte($carte,$section,true);
				$noticesTraitees[] = $carte['halId_s'];
			}
		}
	}
	
	// faire 10 boucles par type et changer la nomenclature pour avoir bien le docType_S idem 3.3 et 3.2.5
	
	$section->writeText('<br /><br />      B.5.1 Diffusion de connaissances (documents destinés à un public large de non spécialistes, ouvrages, films, site web, etc.)',$fontStyleTitre);
	$section->writeText('<br /><br />      B.5.2 Conception et animation de débats avec les citoyens (conférences ou colloques, autres manifestations)',$fontStyleTitre);
	$section->writeText('<br /><br />      B.5.3 Actions d’information scientifique et technique, ouvrage de vulgarisation, contribution au profit d’acteurs publics (citoyen.nes, élu.es, collectivités territoriales) ou privés',$fontStyleTitre);
	$section->writeText('<br /><br />      B.5.4 Organisation de visites de journalistes, de personnalités politiques',$fontStyleTitre);
	
	///////////////////////////////////////////
	/// Ajout de sections supplémentaires pour compatibilité OpenOffice
	////////////////////////////////////////
	//$section = $rtf->addSection();
	
	/////////////////////////////////////////////////////////////////////////
	//
	//   3.      Formation par la recherche, formation initiale et continue
	//
	/////////////////////////////////////////////////////////////////////////
	$section->writeText('<br /><br/>C. Formation par la recherche, formation initiale et continue',$fontStyleTitreBold);
	// 3.1 Contribution à la formation par la recherche
	$section->writeText('<br /><br />   C.1 Contribution à la formation par la recherche',$fontStyleTitre);
	$section->writeText('<br />Précisez le nom de l’étudiant.e, l’année et votre rôle (encadrant, co-encadrant) voire le type de financements.',$fontBleu);
	$section->writeText('<br /><br />      C.1.1 Encadrement d’étudiant.es BAC+2, BAC+3',$fontStyleTitre);
	$section->writeText('<br /><br />      C.1.2 Encadrement d’étudiant.es BAC+4, BAC+5',$fontStyleTitre);
	$section->writeText('<br /><br />      C.1.3 Thèses',$fontStyleTitre);
	$section->writeText('<br /><br />      C.1.4 Post-doctorant.es',$fontStyleTitre);
	$section->writeText('<br /><br />      C.1.5 Autres types d’encadrement',$fontStyleTitre);
	$section->writeText('<br /><br />      C.1.6 Mémoire d’étudiant.e',$fontStyleTitre);
	// 3.2 Contribution à la formation initiale et continue
	$section->writeText('<br /><br />   C.2 Contribution à la formation initiale et continue',$fontStyleTitre);
	$section->writeText('<br /><br />      C.2.1 Contribution aux enseignements',$fontStyleTitre);
	$section->writeText('<br />Intitulé de la formation ; précisez le niveau (Licence, Master ou Ecole doctorale) et la nature de votre investissement (participation à l’enseignement, conception et mise en place d’enseignement). Indiquer le nombre d’heures d’enseignement (moyenne annuelle sur la période).',$fontBleu);
	$section->writeText('<br /><br />      C.2.2 Contribution à la formation destinée à des professionnels',$fontStyleTitre);
	$section->writeText('<br />Intitulé de la formation ; préciser la nature de votre contribution (conception, mise en œuvre, participation). Indiquez le nombre d’heures d’enseignement sur la période (moyenne annuelle sur la période).',$fontBleu);
	$section->writeText('<br /><br />      C.2.3 Contribution à la formation continue des scientifiques ou technicien.nes',$fontStyleTitre);
	$section->writeText('<br />Ecoles chercheur.es, université d’été…',$fontBleu);
	$section->writeText('<br /><br />      C.2.4 Contenus pédagogiques, cours<br />',$fontStyleTitre);
	
	if (isset($cours)){
		foreach($cours as $cour){
			ecritCours($cour,$section);
			$noticesTraitees[] = $cour['halId_s'];	
		}
	}
	
	
	$section->writeText('<br /><br />      C.2.5 Contribution à la diffusion en ligne de la connaissance',$fontStyleTitre);
	$section->writeText('<br />MOOC, jeux sérieux… ; préciser le mode d’accès (notamment, s’il s’agit d’initiatives d’open education).<br />',$fontBleu);
	
	if (isset($logs)){
		foreach($logs as $log){
			if ($log['inra_publicVise_local_s'][0] == "ET"){
				ecritLogiciel($log,$section,true);
				$noticesTraitees[] = $log['halId_s'];
			}
		}
	}
	if (isset($images)){
		foreach($images as $image){
			if ($image['inra_publicVise_local_s'][0] == "ET"){
				ecritImage($image,$section,true);
				$noticesTraitees[] = $image['halId_s'];
			}
		}
	}
	if (isset($sons)){
		foreach($sons as $son){
			if ($son['inra_publicVise_local_s'][0] == "ET"){
				ecritVideoSon($son,$section,true);
				$noticesTraitees[] = $son['halId_s'];
			}
		}
	}
	if (isset($videos)){
		foreach($videos as $video){
			if ($video['inra_publicVise_local_s'][0] == "ET"){
				ecritVideoSon($video,$section,true);
				$noticesTraitees[] = $video['halId_s'];
			}
		}
	}
	if (isset($cartes)){
		foreach($cartes as $carte){
			if ($carte['inra_publicVise_local_s'][0] == "ET"){
				ecritCarte($carte,$section,true);
				$noticesTraitees[] = $carte['halId_s'];
			}
		}
	}
	
	$section->writeText('<br /><br />      C.3 Autres productions en lien avec la formation (ouvrages, chapitres d’ouvrage, communications…)<br />',$fontStyleTitre);
			
	if (isset($articles)){
		foreach($articles as $article){
			if ($article['inra_publicVise_local_s'][0] == "ET"){
				ecritArticle($article,$section,true,true);
				$noticesTraitees[] = $article['halId_s'];
			}	
		}
	}
	if (isset($comms)){
		foreach($comms as $comm){
			if ($comm['inra_publicVise_local_s'][0] == "ET"){
				ecritComm($comm,$section,true);
				$noticesTraitees[] = $comm['halId_s'];
			}
		}
	}
	if (isset($encyclos)){
		foreach($encyclos as $encyclo){
			if ($encyclo['inra_publicVise_local_s'][0] == "ET"){
				ecritChapitreEncyclo($encyclo,$section,true);
				$noticesTraitees[] = $encyclo['halId_s'];
			}
		}
	}
	if (isset($proceedings)){
		foreach($proceedings as $proceeding){
			if ($proceeding['inra_publicVise_local_s'][0] == "ET"){
				ecritProceeding($proceeding,$section,true);
				$noticesTraitees[] = $proceeding['halId_s'];
			}
		}
	}
	if (isset($posters)){
		foreach($posters as $poster){
			if ($poster['inra_publicVise_local_s'][0] == "ET"){
				ecritPoster($poster,$section,true);
				$noticesTraitees[] = $poster['halId_s'];
			}
		}
	}
	if (isset($ouvrages)){
		foreach($ouvrages as $ouvrage){
			if ($ouvrage['inra_publicVise_local_s'][0]=="ET"){
				ecritOuv($ouvrage,$section,true,true);
				$noticesTraitees[] = $ouvrage['halId_s'];
			}
		}
	}
			
	if (isset($chapitres)){
		foreach($chapitres as $chapitre){
			if ($chapitre['inra_publicVise_local_s'][0] == "ET"){
				ecritChapitreEncyclo($chapitre,$section,true);
				$noticesTraitees[] = $chapitre['halId_s'];
			}
		}
	}
	
	if (isset($issues)){
		foreach($issues as $issue){
			if ($issue['inra_publicVise_local_s'][0] == "ET"){
				ecritIssue($issue,$section,true);
				$noticesTraitees[] = $issue['halId_s'];
			}
		}
	}
	
	///////////////////////////////////////////
	/// Ajout de sections supplémentaires pour compatibilité OpenOffice
	////////////////////////////////////////
	//$section = $rtf->addSection();
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//  4. Animation ou direction de collectifs, de grands instruments, de ressources, de programmes ou de réseaux
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$section->writeText('<br />D. Animation ou direction de collectifs, de grands instruments, de ressources, de programmes ou de réseaux',$fontStyleTitreBold);
	$section->writeText('<br /><br />   D.1 Direction d’unité ou d’équipe, responsabilités internes à l’unité',$fontStyleTitre);
	$section->writeText('<br /><br />   D.2 Conception ou responsabilité scientifique de ressources ou de dispositifs collectifs',$fontStyleTitre);
	$section->writeText('<br /><br />      D.2.1 Grands instruments, observatoires, plateformes, collections de ressources biologiques, infrastructures scientifiques collectives et infrastructures de recherche (dont les e-infrastructures)',$fontStyleTitre);
	$section->writeText('<br /><br />      D.2.2 Pilotage ou animation d’instrument ou de ressources collectives pour la recherche, mis à disposition d’une communauté scientifique nationale (au-delà de l’unité) ou internationale',$fontStyleTitre);
	$section->writeText('<br /><br />   D.3 Responsabilité dans des réseaux thématiques ou disciplinaires, nationaux ou internationaux',$fontStyleTitre);
	$section->writeText('<br />Lister les réseaux en précisant leur périmètre institutionnel (acteurs/actrices INRAE, non INRAE, national, international, etc.), la nature de votre participation, et l’année du début de celle-ci.',$fontBleu);
	$section->writeText('<br /><br />   D.4 Animation de communautés (« community manager ») associées au développement de la science ouverte (ex. communauté de développeurs ou d’utilisateurs)',$fontStyleTitre);
	$section->writeText('<br /><br />   D.5 Responsabilités éditoriales et d’organisation de manifestations d’ordre scientifique',$fontStyleTitre);
	$section->writeText('<br />Lister et préciser la nature de vos responsabilités ; indiquer l’année et/ ou la durée de votre engagement.<br />Indiquer si vous êtes « éditeur/éditrice invité.e » pour des numéros spéciaux',$fontBleu);
	$section->writeText('<br /><br />   D.6. Responsabilités dans les structures de formation',$fontStyleTitre);
	$section->writeText('<br />Par exemple : Ecole Doctorale…',$fontBleu);
	$section->writeText('<br /><br />   D.7. Responsabilité ou contribution importante aux activités d\'appui à la recherche au sein des unités ou des départements',$fontStyleTitre);
	$section->writeText('<br />Adjoint Chef.fe de département, chargé.e de partenariat, chargé.e Europe, chargé.e RH, chargé.e de communication, correspondant.e Appui aux Politiques Publiques ...',$fontBleu);
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//  5.  Autres productions
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$section->writeText('<br/><br />E. Autres productions non référencées ou non ventilées par HAL',$fontStyleTitreBold);

	
		
	///////////////////////////////////////////////////////////////////////////
	//
	//  TRAITEMENT DES NOTICES NON VENTILEES
	//
	//////////////////////////////////////////////////////////////////////////
	$section->writeText('<br />Cette nomenclature regroupe l’ensemble des publications qui n’ont pu être ventilées dans aucune des catégories. La publication peut ne correspondre à aucune des catégories définies dans la liste des productions ; ou alors elle n’a pas pu être correctement assignée par HAL INRAE à cause de données incomplètes (métadonnées manquantes).<br />',$fontBleu);
	$nbNoticeNonVentile = 0;
	
	// Articles non ventiles
	if (isset($articles) && isset($noticesTraitees)){
		$nbArticlesNonTraite = getNbNonTraitee($articles,$noticesTraitees);
		if ($nbArticlesNonTraite>0) {
			if ($nbArticlesNonTraite==1) $section->writeText('<br />   Article',$fontStyleTitre);
			else $section->writeText('<br />   Articles',$fontStyleTitre);
			foreach ($articles as $article) {
				if (!in_array($article['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritArticle($article,$section,true,false);
				}
			}
		}
	}
	
	// Communications non ventilees
	if (isset($comms) && isset($noticesTraitees)){
		$nbCommNonTraite = getNbNonTraitee($comms,$noticesTraitees);
		if ($nbCommNonTraite>0) {
			if ($nbCommNonTraite==1) $section->writeText('<br />   Communication dans un congrès',$fontStyleTitre);
			else $section->writeText('<br />   Communications dans un congrès',$fontStyleTitre);
			foreach ($comms as $comm) {
				if (!in_array($comm['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritComm($comm,$section);
				}
			}
		}
	}
	
	// Poster non ventiles
	if (isset($posters) && isset($noticesTraitees)){
		$nbPostersNonTraite = getNbNonTraitee($posters,$noticesTraitees);
		if ($nbPostersNonTraite>0) {
			if ($nbPostersNonTraite==1) $section->writeText('<br />   Poster',$fontStyleTitre);
			else $section->writeText('<br />   Posters',$fontStyleTitre);
			foreach ($posters as $poster) {
				if (!in_array($poster['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritPoster($poster,$section);
				}
			}
		}
	}
	
	// Ouvrages non ventiles
	if (isset($ouvs) && isset($noticesTraitees)){
		$nbOuvNonTraite = getNbNonTraitee($ouvs,$noticesTraitees);
		if ($nbOuvNonTraite>0) {
			if ($nbOuvNonTraite==1) $section->writeText('<br />   Ouvrage',$fontStyleTitre);
			else $section->writeText('<br />   Ouvrages',$fontStyleTitre);
			foreach ($ouvs as $ouv) {
				if (!in_array($ouv['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritOuv($ouv,$section,true,true);
				}
			}
		}
	}
	
	// Chapitre non ventiles
	if (isset($chapitres) && isset($noticesTraitees)){
		$nbChapitreNonTraite = getNbNonTraitee($chapitres,$noticesTraitees);
		if ($nbChapitreNonTraite>0) {
			if ($nbChapitreNonTraite==1) $section->writeText('<br />   Chapitre',$fontStyleTitre);
			else $section->writeText('<br />   Chapitres',$fontStyleTitre);
			foreach ($chapitres as $chapitre) {
				if (!in_array($chapitre['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritChapitreEncyclo($chapitre,$section);
				}
			}
		}
	}	
		
	// Pré-publications non ventilees
	if (isset($prepublis) && isset($noticesTraitees)){
		$nbPrepubNonTraite = getNbNonTraitee($prepublis,$noticesTraitees);
		if ($nbPrepubNonTraite>0) {
			if ($nbPrepubNonTraite==1) $section->writeText('<br />   Pré-publication',$fontStyleTitre);
			else $section->writeText('<br />   Pré-publications',$fontStyleTitre);
			foreach ($prepublis as $prepubli) {
				if (!in_array($prepubli['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritPreprintWp($prepubli,$section,true);
				}
			}
		}
	}
					
	// Rapport non ventiles
	if (isset($rapports) && isset($noticesTraitees)){
		$nbRapportsNonTraite = getNbNonTraitee($rapports,$noticesTraitees);
		if ($nbRapportsNonTraite>0) {
			if ($nbRapportsNonTraite==1) $section->writeText('<br />   Rapport',$fontStyleTitre);
			else $section->writeText('<br />   Rapports',$fontStyleTitre);
			foreach ($rapports as $rapport) {
				if (!in_array($rapport['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritRapport($rapport,$section,true);
				}
			}
		}
	}
	
	// hdr non ventiles
	if (isset($hdr) && isset($noticesTraitees)){
		$nbHdrNonTraite = getNbNonTraitee($hdr,$noticesTraitees);
		if ($nbHdrNonTraite>0) {
			$section->writeText('<br />   HDR',$fontStyleTitre);
			foreach ($hdr as $notice) {
				if (!in_array($notice['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritHdr($notice,$section);
				}
			}
		}
	}
	
	// Logiciels non ventiles
	if (isset($logs) && isset($noticesTraitees)){
		$nbLogNonTraite = getNbNonTraitee($logs,$noticesTraitees);
		if ($nbLogNonTraite>0) {
			if ($nbLogNonTraite==1) $section->writeText('<br />   Logiciel',$fontStyleTitre);
			else $section->writeText('<br />   Logiciels',$fontStyleTitre);
			foreach ($logs as $log) {
				if (!in_array($log['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritLogiciel($log,$section);
				}
			}
		}
	}
	
	// Brevet non ventiles
	if (isset($brevets) && isset($noticesTraitees)){
		$nbBrevetNonTraite = getNbNonTraitee($brevets,$noticesTraitees);
		if ($nbBrevetNonTraite>0) {
			if ($nbBrevetNonTraite==1) $section->writeText('<br />   Brevet',$fontStyleTitre);
			else $section->writeText('<br />   Brevets',$fontStyleTitre);
			foreach ($brevets as $brevet) {
				if (!in_array($brevet['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritBrevet($brevet,$section);
				}
			}
		}
	}
	
	// Video non ventiles
	if (isset($videos) && isset($noticesTraitees)){
		$nbVideoNonTraite = getNbNonTraitee($videos,$noticesTraitees);
		if ($nbVideoNonTraite>0) {
			if ($nbVideoNonTraite==1) $section->writeText('<br />   Video',$fontStyleTitre);
			else $section->writeText('<br />   Vidéos',$fontStyleTitre);
			foreach ($videos as $video) {
				if (!in_array($video['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritVideoSon($video,$section,false);
				}
			}
		}
	}
	
	// Son non ventiles
	if (isset($sons) && isset($noticesTraitees)){
		$nbSonNonTraite = getNbNonTraitee($sons,$noticesTraitees);
		if ($nbSonNonTraite>0) {
			if ($nbSonNonTraite==1) $section->writeText('<br />   Son',$fontStyleTitre);
			else $section->writeText('<br />   Sons',$fontStyleTitre);
			foreach ($sons as $son) {
				if (!in_array($son['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritVideoSon($son,$section,false);
				}
			}
		}
	}
	
	// Images non ventiles
	if (isset($images) && isset($noticesTraitees)){
		$nbImagesNonTraite = getNbNonTraitee($images,$noticesTraitees);
		if ($nbImagesNonTraite>0) {
			if ($nbImagesNonTraite==1) $section->writeText('<br />   Image',$fontStyleTitre);
			else $section->writeText('<br />   Images',$fontStyleTitre);
			foreach ($images as $image) {
				if (!in_array($image['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritImage($image,$section);
				}
			}
		}
	}
	
	// Cartes non ventiles
	if (isset($cartes) && isset($noticesTraitees)){
		$nbCartesNonTraite = getNbNonTraitee($cartes,$noticesTraitees);
		if ($nbCartesNonTraite>0) {
			if ($nbCartesNonTraite==1) $section->writeText('<br />   Carte',$fontStyleTitre);
			else $section->writeText('<br />   Cartes',$fontStyleTitre);
			foreach ($cartes as $carte) {
				if (!in_array($carte['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritCarte($carte,$section);
				}
			}
		}
	}
	
	// Theses toujours dans la rubrique 5.
	if (isset($theses) && isset($noticesTraitees)){
		$nbThesesNonTraite = getNbNonTraitee($theses,$noticesTraitees);
		if ($nbThesesNonTraite>0) {
			if ($nbThesesNonTraite==1) $section->writeText('<br />   Thèse',$fontStyleTitre);
			else $section->writeText('<br />   Thèse',$fontStyleTitre);
			foreach ($theses as $these) {
				if (!in_array($these['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritHdr($these,$section);
				}
			}
		}
	}
	
	// Cours toujours dans la rubrique 5.
	if (isset($cours) && isset($noticesTraitees)){
		$nbThesesNonTraite = getNbNonTraitee($cours,$noticesTraitees);
		if ($nbThesesNonTraite>0) {
			if ($nbThesesNonTraite==1) $section->writeText('<br />   Cours',$fontStyleTitre);
			else $section->writeText('<br />   Cours',$fontStyleTitre);
			foreach ($cours as $cour) {
				if (!in_array($cour['halId_s'], $noticesTraitees)){
					$nbNoticeNonVentile++;
					ecritCours($cour,$section);
				}
			}
		}
	}
	
	// type OTHER non ventilé
	if (isset($others) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($others,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Autre publication',$fontStyleTitre);
			else $section->writeText('<br />   Autres publications',$fontStyleTitre);
			if (isset($others)) {
				foreach ($others as $other) {
					if (!in_array($other['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritOther($other,$section,true,false);
					}
				}
			}
		}
	}
	
	/******************************************************/
	// AJOUT DES NOUVEAUX TYPES 
	/******************************************************/
	// N° spécial de revue
	if (isset($issues) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($issues,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			$section->writeText('<br />   N° spécial de revue',$fontStyleTitre);
			if (isset($issues)) {
				foreach ($issues as $issue) {
					if (!in_array($issue['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritIssue($issue,$section,false);
					}
				}
			}
		}
	}
	
	// type TRAD non ventilé
	if (isset($trads) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($trads,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Traduction',$fontStyleTitre);
			else $section->writeText('<br />   Traductions',$fontStyleTitre);
			if (isset($trads)) {
				foreach ($trads as $trad) {
					if (!in_array($trad['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritTradBlog($trad,$section);
					}
				}
			}
		}
	}
	
	// type BLOG non ventilé
	if (isset($blogs) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($blogs,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Article de blog scientifique',$fontStyleTitre);
			else $section->writeText('<br />   Article de blog scientifique',$fontStyleTitre);
			if (isset($blogs)) {
				foreach ($blogs as $blog) {
					if (!in_array($blog['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritTradBlog($blog,$section);
					}
				}
			}
		}
	}
	
	// type NOTICE non ventilé
	if (isset($encyclos) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($encyclos,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Not. encyclopédie / dictionnaire',$fontStyleTitre);
			else $section->writeText('<br />   Not. encyclopédie / dictionnaire',$fontStyleTitre);
			if (isset($encyclos)) {
				foreach ($encyclos as $encyclo) {
					if (!in_array($encyclo['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritChapitreEncyclo($encyclo,$section);
					}
				}
			}
		}
	}
	
	// type CREPORT non ventilé
	if (isset($creports) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($creports,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Chapitre de rapport',$fontStyleTitre);
			else $section->writeText('<br />   Chapitre de rapport',$fontStyleTitre);
			if (isset($creports)) {
				foreach ($creports as $creport) {
					if (!in_array($creport['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritRapport($creport,$section,true);
					}
				}
			}
		}
	}
	
	// type MEMLIC et MEM non ventilé
	if (isset($memoires)) {
		$section->writeText('<br />   Mémoire',$fontStyleTitre);
			
		foreach ($memoires as $memoire) 
			ecritArticle($memoire,$section);
			
				
	}
	
	/*****************************************************/
	// Autres types non ventiles : Non classés
	/*****************************************************/
	if (isset($autres) && isset($noticesTraitees) ){
		$nbAutresNonTraite = getNbNonTraitee($autres,$noticesTraitees);
		if ($nbAutresNonTraite>0) {
			if ($nbAutresNonTraite==1) $section->writeText('<br />   Non classé',$fontStyleTitre);
			else $section->writeText('<br />   Non classés',$fontStyleTitre);
			if (isset($autres)) {
				foreach ($autres as $autre) {
					if (!in_array($autre['halId_s'], $noticesTraitees)){
						$nbNoticeNonVentile++;
						ecritOther($autre,$section,true,true);
					}
				}
			}
		}
	}
	
		
	$nbNoticeTraite = count($notices);
	$msg .= "<br/>Nombre de publications traitées : ".$nbNoticeTraite. " <br/>Nombre de publications non ventilées : ".$nbNoticeNonVentile."<br/>";
	$msg = "Résultats de l'export :<br />".$msg;
	
	// Calcul des informations pour l'affichage du donut 
	@include_once('../lib/mod_donut.php');
	////////////////////////////////////////////////////	
		
	$myRtf = uniqid($idhal.'_') . '.rtf';
	// enregistrement du fichier rtf
	$rtf->save($dir . '/docs/' . $myRtf);

	$urlFile = "/css/docs/".$myRtf;
	$linkFile = '<br><a href="'. $urlFile. '" target="_blank">Télécharger ma liste de publications (format .rtf OpenOffice)</a><br>';
	$linkFile ="";

    $cvhal = "https://cv.hal.science/".$idhal;
	$linkCvHal = '<a  class="btn btn-secondary" href="'. $cvhal. '" target="_blank">Afficher votre CV HAL</a>';
	
	$retourne[0] =  "<br />".$linkFile ."<br />" .$linkInrae . "<br />" . $linkHAL . "<br />" . $linkCvHal;
    $retourne[1] =  $msg;
	$retourne[2] =  $urlFile;
	
	if (isset($tabres)) {
		$retourne[3] = $labels;
		$retourne[4] = $nbNotices;
		$retourne[5] = $couleurs;
		$retourne[6] = $tabres;
	}
	
	echo json_encode( $retourne );
}	
	
?>