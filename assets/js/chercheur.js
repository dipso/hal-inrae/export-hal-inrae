

function validYear(testDate,annee){
	if (testDate > 1950 &&  testDate<annee)
		return true;
	else
		return false;
}

function efface_liste(){
	$("select option").prop("selected", false);
}


function efface(formeBool){
	$('#resultats').html(' ');
	$('#erreurs').html(' ');
	$('#details').html(' ');
	$('#donutbox').hide();
	$('#tableauresultat').html(' ');
	
}



function lance_recherche(){
	// on commence par effacer les précédents resultats
	efface(false);
	
	if ( document.getElementById("idhal") ) {
		var idhal = $('#idhal').val().trim();
		var words = idhal.split(' ');
		if (words.length>1) {
			$('#erreurs').append("idHAL invalide : Veuillez saisir une chaine de caratères continue SVP.<br />");
			return ;
		}		
		if (idhal === "") {
			$('#erreurs').append("idHAL vide : Veuillez saisir votre idHAL SVP.<br />");
			return ;
		}
	}
	
	if ( document.getElementById("idcoll") ) {
		var idcoll  = $('#idcoll').val().trim();
		var words = idcoll.split(' ');
		if (words.length>1) {
			$('#erreurs').append("Code de collection invalide : Veuillez saisir une chaine de caratères continue SVP.<br />");
			return ;
		}		
		if (idcoll === "") {
			$('#erreurs').append("idcoll vide : Veuillez saisir votre code de collection SVP.<br />");
			return ;
		}
	}
	
	var dated  = $('#datedeb').val().trim();
	var datef  = $('#datefin').val().trim();
	var date_actuelle = new Date();
	var annee_actuelle = date_actuelle.getFullYear();
	
	// on ajoute à champsuppl les champs selectionnés
	var chpsuppl = "";
	$("select[id='champsuppl'] option:selected").each(function() {
          chpsuppl = $(this).val() + '|' + chpsuppl;
		  console.log(chpsuppl);
    }); 	
	
	var datedeb;
	if (dated === "") datedeb = 1950; else datedeb = parseInt(dated,10);
	
	var datefin;
	if (datef === "") datefin = annee_actuelle; else datefin = parseInt(datef,10);
	
	if (datedeb < 1950 ||  datedeb > annee_actuelle) {
		$('#erreurs').html("Veuillez SVP saisir une date début valide<br />");
		return ;
	}
	if (datefin < 1950 ||  datefin > annee_actuelle) {
		$('#erreurs').append("Veuillez SVP saisir une date de fin valide<br />");
		return ;
	}
	
	if (datedeb > datefin) {
		$('#erreurs').append("La date de début doit être inférieure ou égale à la date de fin<br />");
		return ;
	}	
	
	
	// affichage de "Export en cours, veuillez patienter SVP ..."
	$("#chargement").html("Export en cours, veuillez patienter SVP <img src=\"./assets/img/loading.gif\"/>").show(); 
	
		
	if (typeof myChart != 'undefined')
		myChart.destroy();
		

	// interrogation de HAL
	$.ajax({
		url: '../../chercheur/chercheur.php',
		type: 'POST',
		data: 'idcoll=' + idcoll + '&idhal=' + idhal + '&dateDeb=' + datedeb + '&dateFin=' + datefin  + '&champsuppl='+chpsuppl,
		dataType: 'JSON',
		success: function(data, status, xhr){
			$("#chargement").hide();
			
			if (data[2]!="Erreur") {
				
				
				url_fichier = 'https://' + window.location.host + data[2];
				$('#details').html('<a href="' + url_fichier +'" id="btnresultat" target="_blank" class="btn btn-label btn-success"><label><i class="fa fa-download"></i></label>TELECHARGER L\'EXPORT</a><br><br>'); 
			
				$('#details').append(data[0]);
				$('#resultats').show();
				$('#details').show();
							          
				
				
				ctx = document.getElementById('monDonut');

				data_donut = {
				  labels: data[3],
				  datasets: [{
					label: 'Nombre de publications',
					data: data[4],
					backgroundColor: data[5],
					hoverOffset: 4
				  }]
				};
		
				
				if (typeof myChart != 'undefined') myChart.destroy();
					myChart = new Chart(ctx, {
						type: 'doughnut',
						data: data_donut
				});
				
				$('html, body').animate({
					scrollTop: $("#submitbouton").offset().top
				}, 1500);
				
				
				
				$('#donutbox').show();
				$('html').css("overflow-y","auto");  // affichage de l'ascenseur
				$('footer').css("position","relative");
			
				$('#tableauresultat').html(data[6]);                 // #resultats : zone d'affichage des stats en partie gauche sous le donut
				
				
			} else {
				$('#erreurs').html(data[1]);
				$('#erreurs').show();
				
			}
			//console.log(data);			
		},
		error: function(jqXhr, textStatus, errorMessage){
			$("#chargement").hide();
			if (errorMessage=="Internal Server Error")
				$('#resultats').html("Désolé le traitement a échoué. Contactez <a href=\"mailto:hal@inrae.fr\">hal@inrae.fr</a> si le problème persiste.");
			else
				$('#resultats').html(errorMessage);
			$('#resultats').show();
			console.log(textStatus);
		}
	});
}

$(document).ready(function(){	

	$('#donutbox').hide();
	
	// lancement de la recherche sur la touche Entrée
	$('body').keypress(function(e){
		if( e.which == 13 ){
			lance_recherche();
		}
	});	
	
	// Click sur le bouton EXPORTEZ du formulaire
	$("#submitbouton").click(function(){
		lance_recherche();		
	});

	// Click sur vider  la sélection
	$("#linkexpdefaut").click(function(){
		efface_liste();		
	}); 		
	
	// clic sur le bouton EFFACER
	$("#cancelbouton").click(function(){
		$('#idhal').val('');
		$('#datedeb').val('');
		$('#datefin').val('');
		$('#resultats').html(' ');
		$('#resultats').hide();
		$('#details').html(' ');
		$('#erreurs').html(' ');
		$("#equipelabo").val('');
		$("#soulignauteur").prop("checked", false);
		$('#donutbox').hide();
		$("#tableauresultat").html('');
		efface_liste();
	});	
		
	// Cacher le message "Export en cours..."
	//$("#chargement").hide();
	
	
});