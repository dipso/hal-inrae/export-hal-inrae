

$(document).ready(function(){	
	// au chargement, on ferme toutes les reponses : 
	$(".reponse").hide();
	
	$( "#lien1").click(function(){
		$(".reponse").hide();
		//$('#reponse1').hide();
		$('#reponse3').show();
	})
	
	$( "#lien2").click(function(){
		//$('#reponse1').hide();
		$(".reponse").hide();
		$('#reponse5').show();
	})
	
	$( "#lien3").click(function(){
		$(".reponse").hide();
		//$('#reponse13').hide();
		$('#reponse8').show();
	})
	
	$( "#lien4").click(function(){
		$(".reponse").hide();
		//$('#reponse11').hide();
		$('#reponse9').show();
	})
	
	$( "#lien5").click(function(){
		$(".reponse").hide();
		//$('#reponse7').hide();
		$('#reponse8').show();
	})
	
	
	$( "#lien6").click(function(){
		$(".reponse").hide();
		//$('#reponse5').hide();
		$('#reponse9').show();
	})
	
	$( "#lien7").click(function(){
		$(".reponse").hide();
		//$('#reponse5').hide();
		$('#reponse11').show();
	})
	
	$( "#lien8").click(function(){
		$(".reponse").hide();
		$('#reponse21').show();
	})
	
	$( "[id^=question" ).click(function(){
		var question = this.id; //renvoie question2 par ex
		var numero = question.replace("question","");
		
		$(".reponse").hide();  // on cache toutes les réponses pour n'afficher que celle où on a cliqué
		
		if ($('#reponse'+numero).is(':visible'))
			$('#reponse'+numero).hide();
		else
			$('#reponse'+numero).show();
	})
	
	var $videoSrc;  
	$('.video-btn').click(function() {
		$videoSrc = $(this).data( "src" );
	});
	
  
	// when the modal is opened autoplay it  
	$('#myModal').on('shown.bs.modal', function (e) {
		// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
		$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	})
	  


	// stop playing the youtube video when I close the modal
	$('#myModal').on('hide.bs.modal', function (e) {
		// a poor man's stop video
		$("#video").attr('src',$videoSrc); 
	}) 

});