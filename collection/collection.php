<?php
/*******************************************************************************************************************************************/
/*
/*              PROGRAMME D'EXPORT D'UNE COLLECTION POUR INRAE
/*
/*   Usage : appel ajax depuis assets/js/collection.js
/*   Paramatres : collection, année de début, année de fin, booléen soulignement des auteurs et info labo (pour le soulignement des auteurs)
/*   Résultat : production d'un fichier excel dans le répertoire xlsx/ comportant les notices résultantes de l'interrogation de l'API HAL
/*              avec ventilation par onglet pour chaque type de publication
/*   Développement : Pierre Pichard - wouaib.com
/*   Date : 07/2022
/*   MAJ : 12/2022 : Prise en compte des nouveaux types de documents et sous-type standards HAL
/*
/*******************************************************************************************************************************************/


ini_set('memory_limit', '292M');
$debug = false;
if ($debug)
	$h = fopen("debug.log","w");
/** Error reporting */
error_reporting(E_ERROR);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


$noticesTraitees = array();  // stockage de toutes les publis traitées, pour remplir la section des publis non ventilées
$nbNoticeTraite=0;
$liste1 = "halId_s,title_s,authFullName_s,authLastName_s,authFirstName_s,producedDate_s,producedDateY_i,docType_s,docSubType_s,inra_publicVise_local_s,peerReviewing_s,invitedCommunication_s,subTitle_s,bookTitle_s,journalTitle_s,volume_s,issue_s,page_s,publisher_s,doiId_s,uri_s,arxivId_s,biorxivId_s,authorityInstitution_s,number_s,serie_s,conferenceTitle_s,city_s,country_s,conferenceStartDate_s,conferenceEndDate_s,lectureName_s,reportType_s,lectureType_s,submitType_s,openAccess_bool,wosId_s,pubmedId_s,audience_s,otherType_s,authQuality_s,authIdFullName_fs,collCode_s";
$liste2   = "popularLevel_s,authIdHasPrimaryStructure_fs,linkExtId_s,language_s";



// Récuperation des données du formulaire
$collection    = htmlspecialchars(strip_tags($_POST['idcoll']),    ENT_QUOTES);
$anneedeb      = htmlspecialchars(strip_tags($_POST['dateDeb']),  ENT_QUOTES);
$anneefin      = htmlspecialchars(strip_tags($_POST['dateFin']),  ENT_QUOTES);
$champsSuppl   = htmlspecialchars(strip_tags($_POST['champsuppl']), ENT_QUOTES);

if (isset($collection))
	$collection=strtoupper($collection);


// fr_keyword_s|fr_abstract_s|
if (isset($champsSuppl) && $champsSuppl!=""){
	$champsSuppl = str_replace("|",",",$champsSuppl);
	$listeChamps = $liste1.','.$champsSuppl.$liste2;
	$tabChampSupp = explode(',',rtrim($champsSuppl,','));
} else {
	$listeChamps = $liste1.','.$liste2;
}

if ($debug) {
		fwrite($h,print_r($_POST,true));
		fwrite($h,$listeChamps."\n");
	}

@include_once("../lib/functions.php");
@include_once("../lib/functions_hceres.php");
@include_once("../lib/functions_chercheur.php"); // même fonction de remplissage du tableur

if (isset($collection) && isset($anneedeb) && isset($anneefin)) {
  
	$url = "http://api.archives-ouvertes.fr/search/?wt=json&q=collCode_s:".$collection."&rows=100000&fq=producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&fl=".$listeChamps;
	$linkHAL = '<a href="'. $url. '" target="_blank">Afficher la requête API (format json)</a>';
	if ($anneedeb=="") $annedeb="*";
	if ($anneefin=="") $anneefin="*";
	$urlHAL = "https://hal.inrae.fr/search/index/?q=collCode_s:".$collection."+producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&submit=&docType_s=ART+OR+COMM+OR+POSTER+OR+OUV+OR+COUV+OR+DOUV+OR+PATENT+OR+OTHER+OR+UNDEFINED+OR+REPORT+OR+CREPORT+OR+THESE+OR+HDR+OR+LECTURE+OR+MEM+OR+VIDEO+OR+SON+OR+IMG+OR+MAP+OR+SOFTWARE&submitType_s=notice+OR+file+OR+annex&rows=300";
	$linkInrae = '<a href="'. $urlHAL. '" target="_blank">Lancer la recherche sur HAL - INRAE</a>';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if (isset ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
	}
	$resultats = curl_exec($ch);
	curl_close($ch);
	
	$data = json_decode($resultats,true);
	$notices = $data["response"]["docs"];
		
	if (count($notices)==0) {
		$retourne[1] = "Aucune notice ne correspond à ces critères. Veuillez saisir un code de collection correct SVP.";
		$retourne[2] = "Erreur";
		$error = true;
		echo json_encode($retourne);
		exit;
	} else {
		$msg = count($notices)." notices correspondent aux critères";
		$error = false;
	}
	
	$dir = dirname(__FILE__);
	
	if (!isset($champsSuppl)){
		$libelles = array(
		"Type de document","Sous type de document","Auteur","Titre","Revue","Volume","Numéro","Pages","Année","DOI","OA (O/N)","Public visé","Comité de lecture (O/N)","Vulgarisation (O/N)","Titre du congrès","Ville du congrès","Pays du congrès","Date de début du congrès","Date de fin du congrès","Audience","Conférence invitée (O/N)","Titre de l'ouvrage","Titre du volume ou de la collection","Editeur","Nom du cours","Niveau du cours / Niveau de diplôme","Clé UT (WOS)","arXiv id","BioRxiv id","PubMed id","Langue du document","hal-id","Url","Citation"
		);
	} else {
		$strLib = "Type de document,Sous type de document,Auteur,Titre,Revue,Volume,Numéro,Pages,Année,DOI,OA (O/N),Public visé,Comité de lecture (O/N),Vulgarisation (O/N),Titre du congrès,Ville du congrès,Pays du congrès,Date de début du congrès,Date de fin du congrès,Audience,Conférence invitée (O/N),Titre de l'ouvrage,Titre du volume ou de la collection,Editeur,Nom du cours,Niveau du cours / Niveau de diplôme,Clé UT (WOS),arXiv id,BioRxiv id,PubMed id,Langue du document,".$champsSuppl."hal-id,Url,Citation"; 
		$strLib = calculLibelles($strLib);
		$libelles = explode(",",$strLib);
	}
		
	$nbLibelles = count($libelles);		
	if ($debug) fwrite($h,print_r($libelles,true));
	if ($debug) fwrite($h,print_r($tabChampSupp,true));
	
	/** Include PHPExcel **/
	require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';

	// Create new PHPExcel object
	$workbook  = new PHPExcel();
	
	// Ventilation par type de document
	foreach ($notices as $notice) {
		switch($notice['docType_s']){
			case 'ART':		  $articles[]  = $notice;		break;
			case 'REPORT':    $rapports[]  = $notice;       break;
			case 'CREPORT':   $chapitresRapport[]= $notice; break;
			case 'OUV':		  $ouvrages[]  = $notice;		break;
			case 'DOUV':      $douvs[]     = $notice;		break;
			case 'COUV':      $chapitres[] = $notice;		break;
			case 'COMM':      $comms[]     = $notice;		break;
			case 'HDR':		  $hdrs[]      = $notice;		break;					
			case 'OTHER':	  $others[]    = $notice;		break;   // Autre publication scientifique
			case 'SOFTWARE':  $logs[]      = $notice;		break;
			case 'PATENT':    $brevets[]   = $notice;		break;
			case 'LECTURE':   $cours[]     = $notice;		break;
			case 'VIDEO':     $videos[]    = $notice;		break;
			case 'MAP':       $cartes[]    = $notice;		break;
			case 'IMG':       $images[]    = $notice;		break;
			case 'SON':       $sons[]      = $notice;		break;
			case 'POSTER':    $posters[]   = $notice;		break;
			case 'UNDEFINED': $prepublis[] = $notice;		break;
			case 'THESE':	  $theses[]    = $notice;		break;
			case 'MEM':	      $memoires[]  = $notice;		break;
			case 'MEMLIC':	  $memoires[]  = $notice;		break;
			/* Nouveaux types 12/2022 */
			case 'PROCEEDINGS': $proceedings[] = $notice;		break;      // Proceedings/ recueil de communications
			case 'ISSUE':	    $issues[]      = $notice;		break;		// N° spécial de revue/ special issue
			case 'TRAD':	    $trads[]       = $notice;		break;		// Traduction
			case 'BLOG':	    $blogs[]       = $notice;		break;      // Article de blog scientifique
			case 'NOTICE':	    $encyclos[]    = $notice;		break;      // Notice d'encyclopédie ou de dictionnaire
			default :           $autres[]      = $notice;		break;
		}
	}
	
	// ***********************************************************//
	// Remplissage du document Excel
	//
	/////////////////////////////////////////////////////////////////////////////////////////
	// ****************** UN ONGLET pour toutes les notices *******************************//
	
	
	if (!empty($notices)){
		$sheet = $workbook->getActiveSheet();
		$sheet->setTitle('Notices');
		
		// remplissage de la 1ere ligne avec les libelles
		for ($i=0; $i<$nbLibelles; $i++){
			$pCoordinate = PHPExcel_Cell::stringFromColumnIndex($i) . '' . (1);
			$pValue = $libelles[$i];
			$sheet->setCellValue($pCoordinate, $pValue);
		} 
		
	
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($notices as $notice){
			for ($i=0; $i<$nbLibelles; $i++){
			
				$pCoordinate = PHPExcel_Cell::stringFromColumnIndex($i) . '' . ($ligne);
				$pValue = getValue($i,$notice,$tabChampSupp); 
				$sheet->setCellValue($pCoordinate, $pValue);
			}
		   //if ($debug) fwrite($h,print_r($notice),true);
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
		
	
		
	
	$msg = "Résultats de l'export :<br />";
	$msg .= "Nombre de publications traitées : ".$nbNoticeTraite." sur ".count($notices);
	if (count($articles)>0) $msg .= "<br/>Articles : ".count($articles);
	if (count($comms)>0) $msg .= "<br/>Communications : ".count($comms);
	if (count($posters)>0) $msg .= "<br/>Posters : ".count($posters);
	if (count($proceedings)>0) $msg .= "<br/>Proceedings : ".count($proceedings);
	if (count($issues)>0) $msg .= "<br/>N° spécial de revue : ".count($issues);
	if (count($ouvrages)>0) $msg .= "<br/>Ouvrages : ".count($ouvrages);
	if (count($chapitres)>0) $msg .= "<br/>Chapitres d'ouvrage : ".count($chapitres);
	if (count($blogs)>0) $msg .= "<br/>Articles de blog scientifique : ".count($blogs);
	if (count($encyclos)>0) $msg .= "<br/>Not. Encyclopédie dictionnaire : ".count($encyclos);
	if (count($trads)>0) $msg .= "<br/>Traductions : ".count($trads);
	if (count($brevets)>0) $msg .= "<br/>Brevets : ".count($brevets);
	if (count($others)>0) $msg .= "<br/>Autres publications : ".count($others);
	if (count($prepublis)>0) $msg .= "<br/>Preprint, Working Paper : ".count($prepublis);
	if (count($rapports)>0) $msg .= "<br/>Rapports : ".count($rapports);
	if (count($chapitresRapport)>0) $msg .= "<br/>Chapitres de rapport : ".count($chapitresRapport);
	if (count($theses)>0) $msg .= "<br/>Thèses : ".count($theses);
	if (count($hdrs)>0) $msg .= "<br/>HDR : ".count($hdrs);
	if (count($memoires)>0) $msg .= "<br/>Mémoires : ".count($memoires);
	if (count($cours)>0) $msg .= "<br/>Cours : ".count($cours);
	if (count($images)>0) $msg .= "<br/>Images : ".count($images);
	if (count($sons)>0) $msg .= "<br/>Sons : ".count($sons);
	if (count($videos)>0) $msg .= "<br/>Vidéos : ".count($videos);
	if (count($cartes)>0) $msg .= "<br/>Cartes : ".count($cartes);
	if (count($logs)>0) $msg .= "<br/>Logiciels : ".count($logs);
	if (count($autres)>0) $msg .= "<br/>Non classés : ".count($autres);
	
	// Calcul des informations pour l'affichage du donut 
	@include_once('../lib/mod_donut.php');
	////////////////////////////////////////////////////	
	
	$myXls = uniqid($collection.'_') . '.xlsx';
	$writer = new PHPExcel_Writer_Excel2007($workbook);
	$writer->setOffice2003Compatibility(true);
	// enregistrement du fichier xlsx
	$writer->save($dir . '/xlsx/' . $myXls);

	$urlFile = "/collection/xlsx/".$myXls;
	//$linkFile = '<a href="'. $urlFile. '" target="_blank">Télécharger ma liste de publications</a>';    a placer dans  retourne[0] pour avoir un lien sous les boutons
	
	
	$retourne[0] =  "<div style=\"height:5vh;\" />" .$linkInrae . "<br />" . $linkHAL . "</div>";
    $retourne[1] =  $msg;
	$retourne[2] =  $urlFile;
	
	
	// retour de sinfo de mod_donut.php
	$retourne[3] = $labels;
	$retourne[4] = $nbNotices;
	$retourne[5] = $couleurs;
	$retourne[6] = $tabres;
	
	if ($debug) {
		foreach($articles as $article)
			fwrite($h,print_r($article,true));
		fclose($h);
	}
	
	
	echo json_encode( $retourne );
}	
	
?>